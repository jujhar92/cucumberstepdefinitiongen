Feature: Login into application
Description: Test login functionality

Scenario: Login into Dragon application
	Given I open dragon application using testDataKey 'openApp'
	And Login into the application using testDataKey 'loginCredentails'
	And Disable CAM Workflow using testDataKey 'disableCAM'
	And Create quote using testDataKey 'createQuote'
	
	