package com.jbhati.cucumber.stepDefinitions;

import com.jbhati.cucumber.stepHandler.StepHandler;
import cucumber.api.java.en.Given;
import java.lang.String;

public class Policy {
  @Given("^I open my dragon application using testDataKey '([^']*)'$")
  public void IopenmydragonapplicationusingtestDataKey(String testDataKey) {
    StepHandler.executeUserAction(testDataKey);
  }

  @Given("^Login into my application using testDataKey '([^']*)'$")
  public void LoginintomyapplicationusingtestDataKey(String testDataKey) {
    StepHandler.executeUserAction(testDataKey);
  }
}
