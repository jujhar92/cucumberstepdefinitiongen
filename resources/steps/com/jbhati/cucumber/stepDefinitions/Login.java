package com.jbhati.cucumber.stepDefinitions;

import com.jbhati.cucumber.stepHandler.StepHandler;
import cucumber.api.java.en.Given;
import java.lang.String;

public class Login {
  @Given("^I open dragon application using testDataKey '([^']*)'$")
  public void IopendragonapplicationusingtestDataKey(String testDataKey) {
    StepHandler.executeUserAction(testDataKey);
  }

  @Given("^Login into the application using testDataKey '([^']*)'$")
  public void LoginintotheapplicationusingtestDataKey(String testDataKey) {
    StepHandler.executeUserAction(testDataKey);
  }

  @Given("^Disable CAM Workflow using testDataKey '([^']*)'$")
  public void DisableCAMWorkflowusingtestDataKey(String testDataKey) {
    StepHandler.executeUserAction(testDataKey);
  }

  @Given("^Create quote using testDataKey '([^']*)'$")
  public void CreatequoteusingtestDataKey(String testDataKey) {
    StepHandler.executeUserAction(testDataKey);
  }
}
