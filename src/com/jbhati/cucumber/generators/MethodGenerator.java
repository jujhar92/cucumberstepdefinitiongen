package com.jbhati.cucumber.generators;

import javax.lang.model.element.Modifier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jbhati.cucumber.beans.StepMethod;
import com.jbhati.cucumber.constants.Consts;
import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;

public class MethodGenerator {
	
	private static final Logger logger = LogManager.getLogger("MethodGenerator");
	private static ClassName StepHandlerClassName = ClassName.get(Consts.STEP_HANDLER_CLASS_PKG, Consts.STEP_HANDLER_CLASS_NAME);
	
	public static MethodSpec getMethodSpec(StepMethod stepMethod) {
		if(stepMethod != null) {
			logger.info("Generating "+stepMethod.toString());
			
			ClassName annotationClass = getAnnotationClass(stepMethod.getAnnotation(), stepMethod.getStepName());
			
			MethodSpec methodSpec = MethodSpec.methodBuilder(stepMethod.getName())
				    .addModifiers(Modifier.PUBLIC)
				    .addAnnotation(AnnotationSpec.builder(annotationClass).addMember("value","$S", stepMethod.getStepName()).build())
				    //.addAnnotation(annotationClass)
				    .returns(void.class)
				    .addParameter(String.class, "testDataKey")
				    .addStatement("$T.executeUserAction($L)", StepHandlerClassName, "testDataKey")
				    .build();
			return methodSpec;
		}
		else {
			throw new RuntimeException("Null StepMethod object supplied");
		}
		
	}
	
	private static ClassName getAnnotationClass(String annotation , String stepName) {
		if(annotation != null) {
			logger.info("Generating annotation class "+annotation);
			if(annotation.equalsIgnoreCase(Consts.GIVEN_CLASS_NAME)) {
				ClassName Given = ClassName.get(Consts.GIVEN_CLASS_PKG, Consts.GIVEN_CLASS_NAME);
				return Given;
			}
			else if(annotation.equalsIgnoreCase(Consts.THEN_CLASS_NAME)) {
				ClassName Then = ClassName.get(Consts.THEN_CLASS_PKG, Consts.THEN_CLASS_NAME);
				return Then;
			}
			else if(annotation.equalsIgnoreCase(Consts.WHEN_CLASS_NAME)) {
				ClassName When = ClassName.get(Consts.WHEN_CLASS_PKG, Consts.WHEN_CLASS_NAME);
				return When;
			}
			else {
				throw new RuntimeException("Invalid annotation provided : "+annotation);
			}
		}
		else {
			throw new RuntimeException("Null annotation provided");
		}
	}
	
	private static String getAnnotationValue(String annotation , String stepName) {
		if(annotation != null) {
			logger.info("Generating annotation class "+annotation);
			String value = null;
			if(annotation.equalsIgnoreCase(Consts.GIVEN_CLASS_NAME)) {
				value = Consts.GIVEN_CLASS_NAME+"(\""+stepName+"\")";
			}
			else if(annotation.equalsIgnoreCase(Consts.THEN_CLASS_NAME)) {
				value = Consts.THEN_CLASS_NAME+"(\""+stepName+"\")";
			}
			else if(annotation.equalsIgnoreCase(Consts.WHEN_CLASS_NAME)) {
				value = Consts.WHEN_CLASS_NAME+"(\""+stepName+"\")";
			}
			else {
				throw new RuntimeException("Invalid annotation provided : "+annotation);
			}
			return value;
		}
		else {
			throw new RuntimeException("Null annotation provided");
		}
	}

}
