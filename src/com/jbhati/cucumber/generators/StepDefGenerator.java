package com.jbhati.cucumber.generators;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.Modifier;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jbhati.cucumber.beans.StepClass;
import com.jbhati.cucumber.beans.StepMethod;
import com.jbhati.cucumber.constants.Consts;
import com.jbhati.cucumber.readers.ConfigFileReader;
import com.jbhati.cucumber.readers.FeatureReader;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

public class StepDefGenerator {
	
	private static final Logger logger = LogManager.getLogger("StepDefGenerator");
	private static String stepDefDir = ConfigFileReader.getStepDefDir();
	
	public static void main(String[] args) {
		
		if(stepDefDir != null) {
			File stepDefFolder = new File(stepDefDir);
			if(stepDefFolder.isDirectory()) {
				try {
					
					List<StepClass> stepClasses = FeatureReader.getStepClasses();
					
					List<File> javaFiles = new ArrayList<File>();
					
					for(StepClass stepClass: stepClasses) {
						List<StepMethod> methods = stepClass.getMethods();
						List<MethodSpec> methodSpecs = new ArrayList<MethodSpec>();
						for(StepMethod stepMethod: methods) {
							methodSpecs.add(MethodGenerator.getMethodSpec(stepMethod));
						}
						
						TypeSpec stepDefClass = TypeSpec.classBuilder(stepClass.getName())
							    .addModifiers(Modifier.PUBLIC)
							    .addMethods(methodSpecs)
							    .build();
						
						JavaFile javaFile = JavaFile.builder(Consts.STEP_DEF_CLASS_PKG, stepDefClass)
							    .build();
						javaFile.writeTo(stepDefFolder);
						
						//File createJavaFile = new File(stepDefFolder+"/"+Consts.STEP_DEF_JAVA_FOLDER+"/"+stepClass.getName()+".java");
						//javaFiles.add(createJavaFile);
					}
					
					/*JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
					StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
					Iterable<? extends JavaFileObject> compilationUnits1 =
					           fileManager.getJavaFileObjectsFromFiles(javaFiles);
					       compiler.getTask(null, fileManager, null, null, null, compilationUnits1).call();
					
					fileManager.close();*/
					
				} catch (IOException e) {
					logger.error(e.getMessage());
					throw new RuntimeException(e.getMessage());
				}
				
			}
			else {
				logger.error("Path of directory for step definition is file path");
				throw new RuntimeException("Path of directory for step definition is file path");
			}
		}
		else {
			logger.error("No directory for step definition provided in configuration file");
			throw new RuntimeException("No directory for step definition provided in configuration file");
		}
		
		
		
		
		
		

			
			

			
	}

}
