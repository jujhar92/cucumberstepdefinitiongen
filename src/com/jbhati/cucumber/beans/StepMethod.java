package com.jbhati.cucumber.beans;

public class StepMethod {
	private String Name;
	private String annotation;
	private String stepName;
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getAnnotation() {
		return annotation;
	}
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("StepMethod object: [ name : ")
			.append(this.getName())
			.append(" step name : ")
			.append(this.getStepName())
			.append(" annotation : ")
			.append(this.getAnnotation());
		
		return sb.toString();
	}
	
	
}
