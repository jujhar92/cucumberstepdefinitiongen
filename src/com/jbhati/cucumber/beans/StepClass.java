package com.jbhati.cucumber.beans;

import java.util.List;

public class StepClass {

	private String name;
	private List<StepMethod> methods;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<StepMethod> getMethods() {
		return methods;
	}
	public void setMethods(List<StepMethod> methods) {
		this.methods = methods;
	}
	
	

}
