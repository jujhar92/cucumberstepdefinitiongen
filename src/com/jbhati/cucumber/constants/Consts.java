package com.jbhati.cucumber.constants;

public class Consts {
	public static final String GIVEN_CLASS_PKG = "cucumber.api.java.en";
	public static final String WHEN_CLASS_PKG = "cucumber.api.java.en";
	public static final String THEN_CLASS_PKG = "cucumber.api.java.en";
	public static final String STEP_HANDLER_CLASS_PKG = "com.jbhati.cucumber.stepHandler";
	
	public static final String GIVEN_CLASS_NAME = "Given";
	public static final String WHEN_CLASS_NAME = "When";
	public static final String THEN_CLASS_NAME = "Then";
	public static final String STEP_HANDLER_CLASS_NAME = "StepHandler";
	public static final String AND = "And";
	
	public static final String STEP_DEF_CLASS_PKG = "com.jbhati.cucumber.stepDefinitions";
	
	public static final String STEP_DEF_JAVA_FOLDER = "com/jbhati/cucumber/stepDefinitions";
	
	public static final String INPUT_REGEX = "'([^']*)'$";
	
}
