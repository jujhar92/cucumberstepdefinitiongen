package com.jbhati.cucumber.readers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConfigFileReader {
	
	private static final Logger logger = LogManager.getLogger("ConfigFileReader");
	private static Properties properties;
	private static final String propertyFilePath= "./resources/configs/Configuration.properties";
	
	static {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}		
	}
	
	private ConfigFileReader(){
		// restrict instance
	}
	
	
	public static String getFeatureDir() {
		String featuresDir = properties.getProperty("cucumber.features");
		if(featuresDir != null) {
			return featuresDir;
		}
		else {
			throw new RuntimeException("cucumber.features not specified in the Configuration.properties file.");
		}
	}
	
	public static String getStepDefDir() {
		String stepDefDir = properties.getProperty("cucumber.step.definitions");
		if(stepDefDir != null) {
			return stepDefDir;
		}
		else {
			throw new RuntimeException("cucumber.step.definitions not specified in the Configuration.properties file.");
		}
	}
}
