package com.jbhati.cucumber.readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jbhati.cucumber.beans.StepClass;
import com.jbhati.cucumber.beans.StepMethod;
import com.jbhati.cucumber.constants.Consts;

public class FeatureReader {
	
	private static final Logger logger = LogManager.getLogger("FeatureReader");
	private static String featuresDir = ConfigFileReader.getFeatureDir();
	
	public static List<StepClass> getStepClasses(){
		List<StepClass> classes = new ArrayList<StepClass>();
		
		if(featuresDir != null) {
			File folder = new File(featuresDir);
			if(folder.isDirectory()) {
				File[] fileNames = folder.listFiles();
				for(File file : fileNames) {
					if(!file.isDirectory()) {
						try {
							classes.add(getStepClass(file));
		                 } catch (IOException e) {
		                     logger.error(e.getMessage());
		                 }
					}
				}
			}
			else {
				logger.error("Define folder path for features instead of file.");
			}
		}
		else {
			logger.error("Empty feature folder defined");
		}
		
		return classes;
	}
	
	private static StepMethod getStepMethod(String annotation, String input, boolean isAnd) {
		
	   if(isAnd) {
		   input = input.replaceFirst(Consts.AND, "");
	   }
	   else {
		   input = input.replaceFirst(annotation, "");
	   }
	   
	   input = input.trim();
	   
 	   //int len = input.length();
 	   int indexOfKeyStart = input.indexOf("'");
 	   
 	   //String dataKey = input.substring(indexOfKeyStart, len);
 	   
 	   String methodName = input.substring(0,indexOfKeyStart);
 	   
 	   StringBuilder stepName = new StringBuilder();
 	   stepName.append("^")
 	   			.append(methodName)
 	   			.append(Consts.INPUT_REGEX);
 	   
 	   StepMethod stepMethod = new StepMethod();
 	   stepMethod.setStepName(stepName.toString());
 	   stepMethod.setName(methodName.replaceAll("\\s+",""));
 	   stepMethod.setAnnotation(annotation);
 	   
 	   return stepMethod;
		
	}
	
	public static StepClass getStepClass(File file) throws IOException{
        logger.info("reading feature file " + file.getCanonicalPath());
        
        String fileName = file.getName();
        
        if(fileName.contains(".")) {
        	int ind = fileName.indexOf(".");
        	fileName = fileName.substring(0,ind);
        }
        
        String firstChar = String.valueOf(fileName.charAt(0)).toUpperCase();
        String className = firstChar+ fileName.substring(1, fileName.length());
        
        StepClass stepClass = new StepClass();
        stepClass.setName(className);
        
        List<StepMethod> stepMethods = new ArrayList<StepMethod>();
        stepClass.setMethods(stepMethods);
        
        String lastAnnotation = null;
        
        try(BufferedReader br  = new BufferedReader(new FileReader(file))){
              String strLine;
              while((strLine = br.readLine()) != null){
               logger.info("Feature : "+fileName+" Reading feature line is - " + strLine);
               strLine = strLine.trim();
               
               if(strLine.startsWith(Consts.GIVEN_CLASS_NAME)) {
            	   lastAnnotation = Consts.GIVEN_CLASS_NAME;
            	   stepMethods.add(getStepMethod(lastAnnotation, strLine,false));
            	   
               }
               else if(strLine.startsWith(Consts.WHEN_CLASS_NAME)) {
            	   lastAnnotation = Consts.WHEN_CLASS_NAME;
            	   stepMethods.add(getStepMethod(lastAnnotation, strLine,false));
               }
               else if(strLine.startsWith(Consts.THEN_CLASS_NAME)) {
            	   lastAnnotation = Consts.THEN_CLASS_NAME;
            	   stepMethods.add(getStepMethod(lastAnnotation, strLine,false));
               }
               else if(strLine.startsWith(Consts.AND)) {
            	   stepMethods.add(getStepMethod(lastAnnotation, strLine,true));
               }
               else {
            	   logger.error("Unsupported tag used in feature file :"+file.getName());
               }
               
              }
        }
        
        return stepClass;
    }

}
